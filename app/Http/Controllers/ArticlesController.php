<?php

namespace App\Http\Controllers;

use App\Models\Articles;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ArticlesController extends Controller
{
    
    public function index()
    {

        return view('index');
    }

    public function admin_index()
    {
        

        return view('admin.index');
    }

    public function articles_index()
    {
        

        return view('admin.articles.show');
    }

    public function article()
    {
        
        return view('admin.articles.create');
    }

    public function article_create(Request $request)
    {
        $request->validate([
            'userid' => 'required',
            'title' => 'required|string|max:255',
            'content' => 'required',
            'category' => 'required',
            'unit' => 'required',
        ]);

        $request['slug'] = Str::slug($request->title);
        $input = $request->all();

        Articles::create($input);
        return redirect()->back()->with('status','Article was successfully added');
    }

    public function article_show()
    {
        $article = Articles::all();
        return view('Admin.Articles.show', compact('article'));
    }
    
    public function article_edit($id)
    {
        $article = Articles::find($id);
        return view('admin.articles.edit', compact('article'));
    }

    public function update(Request $request)
    {
      $id = $request->input('id');
      $title = $request->input('title');
      $content = $request->input('content');
     
      DB::update("update articles set title = '$title', slug = '$title', content = '$content' where id = '$id'");
      return redirect()->back()->with('status','Article was successfully Updated');
    }

    public function destroy($id)
    {
      $article = Articles::find($id);
      $article_id = $article->id;
      DB::table('Articles')->where('id', '=', $article->id)->delete();
      return redirect()->back()->with('status','Article was successfully delete');
    }
}
