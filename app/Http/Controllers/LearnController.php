<?php

namespace App\Http\Controllers;

use App\Models\Articles;
use App\Models\Category;
use Carbon\Carbon;
use App\Models\LearnRec;
use App\Models\Learn;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class LearnController extends Controller
{
    public function index()
    {
        return view('learner.index');
    }

    public function module($id)
    {
        $article = Articles::where('category', $id)->get();
        $user = Auth::user();
        $user_id = $user->id;
           if($article) {
            $start_date = $current = Carbon::now(); 
            
            $test = LearnRec::where('user_id', $user_id)->where('module_id', $id)->first();
            if($test) {
                
            }else { 
                $New = new LearnRec();
                $New->user_id = $user_id;
                $New->start_date = $start_date;
                $New->module_id = $id;
                $New->save();
                return redirect()->back()->with('status','You have added this category to your learning list.');
            }
            
        $title = Category::find($id);
        return view('learner.categories', compact('article','title')); 
        }  
    }

    public function singlepage($id) {
        $article = Articles::find($id);
        $increment = DB::table('categories')
            ->join('articles', 'categories.id', '=', 'articles.category')
            ->where('articles.category', '=', $article->category)
            ->count();
            $test = Learn::where('article_id', $id)->first();
            $articles = LearnRec::where('module_id', $article->category)->first();
        if ($test) {
            if ($article) {
                $user = Auth::user();
                $user_id = $user->id;
                $id = $article->id;
                $number = $articles->total_articles;
                $count = ++$number;
    
                if ($articles->total_articles == $increment ) {
                    $end_date = now();
                    DB::table('learn_recs')
                        ->where('user_id', $user_id)
                        ->update(['end_date' => $end_date]);
                        return redirect()->back()->with('status','You have completed learning a module.');    
                } else {
                    DB::table('learn_recs')
                        ->where('user_id', $user_id)
                        ->where('module_id', $article->category)
                        ->update(['total_articles' => $count]);
                }
            }
        } else {
            $user = Auth::user();
            $user_id = $user->id;
    
            $newRecord = new Learn();
            $newRecord->user_id = $user_id;
            $newRecord->module_id = $article->category;
            $newRecord->article_id = $id;
            $newRecord->article_count = 1;
            $newRecord->save();
        }
        return view('learner.single_page', compact('article'));   
    }
    
    public function search(Request $request) {
        $search = $request->get('search');
        $article = DB::table('articles')->where('title', 'like', '%'.$search.'%')->paginate(5);
        return view('search', compact('article'));
    }

    public function search_category(Request $request) {
        $search = $request->get('search');
        $article = DB::table('articles')->where('category', 'like', '%'.$search.'%')->paginate(5);
        return view('search', compact('article'));
    }

    public function mylearnings() {
        return view('learner.mylearnings');
    }
}
