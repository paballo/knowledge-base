<?php

namespace App\Http\Controllers;

use App\Models\Articles;
use App\Models\Category;
use Carbon\Carbon;
use App\Models\Record;
use App\Models\UserRecord;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class SinglePageController extends Controller
{
    
    public function index()
    {
        return view('user.index');
    }

    public function module($id)
    {
        $article = Articles::find($id)->get();
        $user = Auth::user();
        $user_id = $user->id;
           if($article) {
            $start_date = $current = Carbon::now(); 
            $count = DB::table('categories')
            ->join('articles', 'categories.id', '=', 'articles.category')
            ->where('articles.category', '=', $id)
            ->count();

            $record = Record::OrderBy('created_at')->where('user_id', '=', $user_id)->get();
            foreach($record as $record)
            if($user->id == $record->user_id) {
               
            }
            else {
                $New = new Record();
                $New->user_id = $user_id;
                $New->start_date = $start_date;
                $New->module_id = $id;
                $New->total_articles = $count;
                $New->save();
            }
        $title = Category::find($id);
           return view('user.categories', compact('article','title'));  

        }  
    }

    public function singlepage($id) {
        $article = Articles::find($id);
        $articles = DB::table('articles')
         ->join('records', 'articles.id', '=', 'records.module_id')
         ->where('records.module_id', '=', $id)
         ->get();
         foreach($articles as $article)
         if ($article) {
            $user = Auth::user();
            $user_id = $user->id;
            $id = $article->id;
            $number = $article->total_articles;
            $count =  -- $number ;
            if($article->total_articles == 0) {
                $end_date = $current = Carbon::now(); 
                DB::update("update records set end_date = '$end_date'  where user_id = '$user_id'");
            }else {
                DB::update("update records set total_articles = '$count'  where user_id = '$user_id' AND id = '$id'");  
            }
            
        }
        if($article) {
               $record = DB::select("select * from user_records ");
               foreach($record as $record)
               if($article->id !== $record->article_id) {
                $user = Auth::user();
                $user_id = $user->id;
                $New = new UserRecord();
                $New->user_id = $user_id;
                $New->module_id = $article->category;
                $New->article_id = $id;
                $New->article_count = 1;
                $New->save(); 
            } else {
             
            }  
            
        }
       return view('user.single_page', compact('article'));
    }

    public function search(Request $request) {
        $search = $request->get('search');
        $article = DB::table('articles')->where('title', 'like', '%'.$search.'%')->paginate(5);
        return view('search', compact('article'));
    }

    public function search_category(Request $request) {
        $search = $request->get('search');
        $article = DB::table('articles')->where('category', 'like', '%'.$search.'%')->paginate(5);
        return view('search', compact('article'));
    }

    public function mylearnings() {
        return view('user.mylearnings');
    }
}
