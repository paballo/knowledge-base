<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CategoryController extends Controller
{
    public function category_index()
    {

        return view('admin.categories.show');
    }
    
    public function index()
    {

        return view('admin.categories.create');
    }

    public function category_create(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
        ]);
        $request['slug'] = Str::slug($request->name);
        $input = $request->all();

        Category::create($input);
        return redirect()->back()->with('status','Category was successfully added');
    }

    public function category_show()
    {
        $category = Category::all();
        return view('admin.categories.show', compact('category'));
    }

    public function category_edit($id)
    {
        $category = Category::find($id);
        return view('admin.categories.edit', compact('category'));
    }

    public function update(Request $request)
    {
      $id = $request->input('id');
      $name = $request->input('name');
     
      DB::update("update categories set name = '$name', slug = '$name' where id = '$id'");
      return redirect()->back()->with('status','Category was successfully updated');
    }

    public function destroy($id)
    {
        $category = Category::find($id);
        $category->delete();
        return redirect()->back()->with('status','Category was successfully deleted');
    }

    
}
