<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LearnRec extends Model
{
    use HasFactory;
    protected $fillable = [
        'user_id',
        'module_id',
        'start_date',
        'end_date',
    ];
}
