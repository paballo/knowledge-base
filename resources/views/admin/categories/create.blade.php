@extends('/admin/layout/app')

@section('content')
<script src="https://kit.fontawesome.com/a076d05399.js" crossorigin="anonymous"></script>
 <script src="https://cdn.tiny.cloud/1/ofq3esqpn0ltymb89297z0b11mfgzn10tolcmt6bqi8ohvff/tinymce/6/tinymce.min.js" referrerpolicy="origin"></script>

<main>
    <header class="page-header page-header-compact page-header-light border-bottom bg-white mb-4">
        <div class="container-xl px-4">
            <div class="page-header-content">
                <div class="row align-items-center justify-content-between pt-3">
                    <div class="col-auto mb-3">
                        <h1 class="page-header-title">
                            <div class="page-header-icon"><i class="fa fa-plus"></i></div>
                            Add Category
                        </h1>
                    </div>
                    <div class="col-12 col-xl-auto mb-3">
                        <a href="/admin" class="btn btn-sm btn-light text-primary" >
                            <i class="fa fa-house"></i> &nbsp;
                            Dashboard
                        </a>
                        
                        <a href="/admin/categories/show" class="btn btn-sm btn-light text-primary" >
                            <i class="fa fa-list"></i>
                            &nbsp; Manage Categories
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </header>
    @if(Session::has('status'))
    <div class="alert alert-info" role="alert">
       <h1 style="color: green">{{Session::get('status')}}</h1>
    </div> 
   @endif
    <!-- Main page content-->
    <div class="container-x1 px-5 mt-5">
        <form action="{{ route('create') }}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="card ">
                <div class="card-header card-header-primary">
                  <h4 class="card-title">{{ __('Add Category') }}</h4>
                  <p class="card-category"></p>
                </div>
                <div class="card-body ">
                  <div class="row">
                    <label class="col-sm-2 col-form-label">{{ __('Category') }}</label>
                    <div class="col-sm-7">
                      <div class="form-group{{ $errors->has('Category') ? ' has-danger' : '' }}">
                        <input class="form-control{{ $errors->has('Category') ? ' is-invalid' : '' }}" name="name"  type="text" placeholder="{{ __('Category') }}" value="" required="true" aria-required="true" />
                      </div>
                    </div>
                  </div>
                </div>
                <div class="card-footer ml-auto mr-auto">
                  <button type="submit" class="btn btn-primary">{{ __('Submit') }}</button>
                </div>
              </div>
        </form>  
    </div>

</main>

@endsection