@extends('Admin.layout.app')

@section('content')

<main>
    <header class="page-header page-header-compact page-header-light border-bottom bg-white mb-4">
        <div class="container-fluid px-4">
            <div class="page-header-content">
                <div class="row align-items-center justify-content-between pt-3">
                    <div class="col-auto mb-3">
                        <h1 class="page-header-title">
                            <div class="page-header-icon"><i class="fa fa-list"></i></div>
                            Categories List
                        </h1>
                    </div>
                    <div class="col-12 col-xl-auto mb-3">
                        <a href="/admin" class="btn btn-sm btn-light text-primary" >
                            <i class="fa fa-house"></i> &nbsp;
                            Home
                        </a>
                        
                        <a href="/admin/categories/create" class="btn btn-sm btn-light text-primary" >
                            <i class="fa fa-plus"></i> &nbsp;
                            Add new categories
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </header>
    @if(Session::has('status'))
     <div class="alert alert-info" role="alert">
        <h1 style="color: green">{{Session::get('status')}}</h1>
     </div> 
    @endif
    <!-- Main page content-->
    <div class="container-fluid px-4">
        <div class="card">
            <div class="card-body">
            @if(Request::get('categories') == 'create')
            @include('admin.categories.create')
            @else
                <table id="datatablesSimple">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Category</th>
                            <th>Created-Date</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>#</th>
                            <th>Category</th>
                            <th>Created-Date</th>
                            <th>Action</th>
                        </tr>
                    </tfoot>
                    <tbody>
                        <?php $catergories = App\Models\Category::select(['id','slug','created_at'])->get(); ?>
                        @if(count($catergories) > 0) {
                            @foreach($catergories as $category)
                            <tr>
                                <td style="font-weight:bold;text-align:center;color:grey;font-size:25">0<?php 
                                   static $num = 0;
                                   echo (++$num)
                                 ?>
                                 </td>
                                   <td>{{$category->slug}}</td>
                                   <td>{{$category->created_at}}</td>
                                   <td>
                                       <a class="btn btn-datatable btn-icon btn-transparent-dark" href="/admin/categories/edit/{{$category->id}}"><i class="fa fa-pencil" style="font-size:15"></i></button>
                                       <a class="btn btn-datatable btn-icon btn-transparent-dark" href="/admin/categories/{{$category->id}}"><i class="fa fa-trash" style="font-size:15"></i></a>
                                   </td>
                             </tr>
                             @endforeach
                        }
                        @else
                        @endif
                    </tbody>
                </table>
                @endif
            </div>

        </div>
    </div>

</main>
@endsection