@extends('Admin.layout.app')

@section('content')

<main>
    <header class="page-header page-header-compact page-header-light border-bottom bg-white mb-4">
        <div class="container-fluid px-4">
            <div class="page-header-content">
                <div class="row align-items-center justify-content-between pt-3">
                    <div class="col-auto mb-3">
                        <h1 class="page-header-title">
                            <div class="page-header-icon"><i class="fa fa-list"></i></div>
                            CATEGORIES LIST
                        </h1>
                    </div>
                    <div class="col-12 col-xl-auto mb-3">
                        <a href="/admin?articles=show" class="btn btn-sm btn-light text-primary" >
                            <i class="fa fa-file-text-o"></i> &nbsp;
                            Articles
                        </a>
                        
                        <a href="/admin?categories=show" class="btn btn-sm btn-light text-primary" >
                            <i class="fa fa-tags"></i> &nbsp;
                            Categories
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- Main page content-->
    <div class="container-fluid px-4">
        <div class="card">
            <div class="card-body">
            @if(Request::get('articles') == 'show')
            @include('admin.articles.show')
            @elseif(Request::get('categories') == 'show')
            @include('admin.categories.show')
            @else
            <table id="datatablesSimple">
                <thead>
                    <tr>
                       <th>#</th>
                        <th>Category</th>
                        <th>Number of Articles</th>
                        <th>Created-Date</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>#</th>
                        <th>Category</th>
                        <th>Number of Articles</th>
                        <th>Created-Date</th>
                    </tr>
                </tfoot>
                <tbody>
                    <?php $catergories = App\Models\Category::select(['id','slug','created_at'])->get(); ?>
                    @if(count($catergories) > 0) {
                     @foreach ($catergories as $item)
                   <tr>
                     <td style="font-weight:bold;text-align:center;color:grey;font-size:25">0<?php 
                        static $num = 0;
                        echo (++$num)
                      ?>
                      </td>
                    <td> <a href="/categories/{{$item->id}}">
                        <i class="fa fa-folder" style="color: orange;font-size:15px">&nbsp; <span style="font-size:15px;text-transform: uppercase;color:black">{{$item->slug}}</span></i>
                    </a></td>
                    <td><?php
                        $count = DB::table('articles')
                        ->where('articles.category', '=', $item->id )
                        ->count();
                       ?>
                      <span class="cat-count">({{$count}})</span></td>
                    <td>{{$item->created_at}}</td>
                </tr>
                @endforeach
            }@else 
            @endif
                </tbody>
            </table>
                @endif
            </div>
        </div>
    </div>
 
</main>

<script src="https://cdn.jsdelivr.net/npm/simple-datatables@latest" crossorigin="anonymous"></script>
<script src="https://sb-admin-pro.startbootstrap.com/js/datatables/datatables-simple-demo.js"></script>
<script src="https://assets.startbootstrap.com/js/sb-customizer.js"></script>
<script type="text/javascript">(function(){window['__CF$cv$params']={r:'71df4c682ebe9cec',m:'nlyqlIPQj.HTCd3.Qcx81jx1rk2TmVqWP2M3ImQEs_Q-1655673519-0-Aag8fQzkLfVEPlnuqjVvzw7PNgkZ/TtyVdDQM2cV9JSbg9UlSjZN4Uy5he4VDLMN/L1J2SN7XR4LKeFdr9PqFmI/HbCB6bBvgxpcBneTfpwrMGYo8y9HScLcVC1jmPWtVL1l3zQqp0cLGbqNraZ3oT9eEVM3xI/TDlOPEnGY+/Ed',s:[0xa87d26d135,0xc6d4f6bf8b],u:'/cdn-cgi/challenge-platform/h/g'}})();</script><script defer src="https://static.cloudflareinsights.com/beacon.min.js/v652eace1692a40cfa3763df669d7439c1639079717194" integrity="sha512-Gi7xpJR8tSkrpF7aordPZQlW2DLtzUlZcumS8dMQjwDHEnw9I7ZLyiOj/6tZStRBGtGgN6ceN6cMH8z7etPGlw==" data-cf-beacon='{"rayId":"71df4c682ebe9cec","token":"6e2c2575ac8f44ed824cef7899ba8463","version":"2022.6.0","si":100}' crossorigin="anonymous"></script>
@endsection