@extends('/admin/layout/app')

@section('content')
<script src="https://kit.fontawesome.com/a076d05399.js" crossorigin="anonymous"></script>
 <script src="https://cdn.tiny.cloud/1/ofq3esqpn0ltymb89297z0b11mfgzn10tolcmt6bqi8ohvff/tinymce/6/tinymce.min.js" referrerpolicy="origin"></script>

<main>
    <header class="page-header page-header-compact page-header-light border-bottom bg-white mb-4">
        <div class="container-xl px-4">
            <div class="page-header-content">
                <div class="row align-items-center justify-content-between pt-3">
                    <div class="col-auto mb-3">
                        <h1 class="page-header-title">
                            <div class="page-header-icon"><i class="fa fa-plus"></i></div>
                            Add Article
                        </h1>
                    </div>
                    <div class="col-12 col-xl-auto mb-3">
                        <a href="/admin "class="btn btn-sm btn-light text-primary">
                            <i class="fa fa-house"></i> &nbsp;
                            Dashboard
                        </a>
                        
                        <a href="/admin/articles/show" class="btn btn-sm btn-light text-primary" >
                            <i class="fa fa-list"></i>
                            &nbsp; Manage Articles
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </header>
    @if(Session::has('status'))
     <div class="alert alert-info" role="alert">
        <h1 style="color: green">{{Session::get('status')}}</h1>
     </div> 
    @endif
    <!-- Main page content-->
    <div class="container-x1 px-5 mt-5">
        <form action="/admin/articles/create" method="post" enctype="multipart/form-data">
            @csrf
            <div class="card ">
                <div class="card-header card-header-primary">
                  <h4 class="card-title">{{ __('Create an Article') }}</h4>
                  <p class="card-category"></p>
                </div>
                <div class="card-body ">
                  <div class="row">
                    <label class="col-sm-2 col-form-label">{{ __('Tittle') }}</label>
                    <div class="col-sm-7">
                      <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                        <input type="hidden" value="{{Auth::user()->id}}" name="userid" />
                        <input class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="title"  type="text" placeholder="{{ __('Tittle') }}" value="" required="true" aria-required="true" />
                      </div>
                    </div>
                  </div>
                  <br>
                  <div class="row">
                    <label for="tag" class="col-sm-2 col-form-label">catergory</label>
                    <div id="select" class="col-sm-7">
                        <select name="category" class="form-control">
                         <?php $catergories = App\Models\Category::select(['id','slug'])->get(); ?>
                       <option >Select cateragory...</option>
                       @foreach ($catergories as $item)
                        <option value="{{$item->id}}">{{$item->slug}}</option>
                       @endforeach
                        </select>
                    </div>
                  </div>
                  <br>
                  <div class="row">
                    <label for="tag" class="col-sm-2 col-form-label">Unit</label>
                    <div id="select" class="col-sm-7">
                        <select name="unit" class="form-control">
                          <option value="">Select unit ...</option>
                         <option value="0">General</option>
                         <option value="1">Reasearch Unit</option>
                         <option value="2">System Development unit</option>
                         <option value="3">Software Development unit</option>
                        </select>
                    </div>
                  </div>
                  <br/>
                  <div class="row">
                    <label class="col-sm-2 col-form-label">{{ __('Content') }}</label>
                    <div class="col-sm-7">
                      <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                        <textarea class="md-textarea form-control" rows="5" name="content">
                        </textarea>
                        <script>
                          tinymce.init({
                            selector: 'textarea',
                            plugins: 'preview importcss searchreplace autolink autosave save directionality code visualblocks visualchars fullscreen image link media template codesample table charmap pagebreak nonbreaking anchor insertdatetime advlist lists wordcount help charmap quickbars emoticons',
                             toolbar: 'undo redo | bold italic underline strikethrough | fontfamily fontsize blocks | alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist | forecolor backcolor removeformat | pagebreak | charmap emoticons | fullscreen  preview save print | insertfile image media template link anchor codesample | ltr rtl',
                            toolbar_mode: 'floating',
                            tinycomments_mode: 'embedded',
                            tinycomments_author: 'Author name',
                          });
                        </script>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="card-footer ml-auto mr-auto">
                  <button type="submit" class="btn btn-primary">{{ __('Submit') }}</button>
                </div>
              </div>
        </form>  
    </div>
</main>

@endsection