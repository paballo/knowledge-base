@extends('admin.layout.app')

@section('content')

<main>
    <header class="page-header page-header-compact page-header-light border-bottom bg-white mb-4">
        <div class="container-fluid px-4">
            <div class="page-header-content">
                <div class="row align-items-center justify-content-between pt-3">
                    <div class="col-auto mb-3">
                        <h1 class="page-header-title">
                            <div class="page-header-icon"><i class="fa fa-list"></i></div>
                            Articles List
                        </h1>
                    </div>
                    <div class="col-12 col-xl-auto mb-3">
                        <a href="/admin" class="btn btn-sm btn-light text-primary" >
                            <i class="fa fa-house"></i> &nbsp;
                            Home
                        </a>
                        <a href="/admin/articles/create" class="btn btn-sm btn-light text-primary" >
                            <i class="fa fa-plus"></i> &nbsp;
                            Add new articles
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </header>
    @if(Session::has('status'))
     <div class="alert alert-info" role="alert">
        <h1 style="color: green">{{Session::get('status')}}</h1>
     </div> 
    @endif
    <div class="container-fluid px-4">
        <div class="card">
            <div class="card-body">
            @if(Request::get('articles') == 'create')
            @include('admin.articles.create')
            @elseif(Request::get('a') == 'edit')
            @include('admin.articles.show')
            @else
                <table id="datatablesSimple">
                    <thead>
                        <tr>
                            <th>#</th>
                           <th>Title</th>
                            <th>Category</th>
                            <th>Content</th>
                            <th>Created-Date</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>#</th>
                            <th>Title</th>
                            <th>Category</th>
                            <th>Content</th>
                            <th>Created-Date</th>
                            <th>Action</th>
                        </tr>
                    </tfoot>
                    <tbody>
                        <?php $articles = App\Models\Articles::select(['id','title','category','content','created_at'])->get(); ?>
                        @if(count($articles) > 0) {
                         @foreach($articles as $article)
                       <tr>
                         <td style="font-weight:bold;text-align:center;color:grey;font-size:25">0<?php 
                            static $num = 0;
                            echo (++$num)
                          ?>
                        <td>{{$article->title}}</td>
                        <?php $cat = App\Models\Category::where('id',$article->category)->first() ?>
                        <td>{{$cat->name}}</td>
                        <td>{!! Str::limit($article->content, 500) !!}</td>
                        <td>{{$article->created_at}}</td>
                        <td>
                            <a class="btn btn-datatable btn-icon btn-transparent-dark" href="/admin/articles/edit/{{$article->id}}"><i class="fa fa-pencil" style="font-size:15"></i></button>
                            <a class="btn btn-datatable btn-icon btn-transparent-dark" href="/admin/articles/{{$article->id}}"><i class="fa fa-trash" style="font-size:15"></i></a>
                        </td>
                    </tr>
                    @endforeach
                }@else
                 @endif
                    </tbody>
                </table>
                @endif
            </div>

        </div>
    </div>
</main>
<script src="https://cdn.jsdelivr.net/npm/simple-datatables@latest" crossorigin="anonymous"></script>
<script src="https://sb-admin-pro.startbootstrap.com/js/datatables/datatables-simple-demo.js"></script>
<script src="https://assets.startbootstrap.com/js/sb-customizer.js"></script>
<script type="text/javascript">(function(){window['__CF$cv$params']={r:'71df4c682ebe9cec',m:'nlyqlIPQj.HTCd3.Qcx81jx1rk2TmVqWP2M3ImQEs_Q-1655673519-0-Aag8fQzkLfVEPlnuqjVvzw7PNgkZ/TtyVdDQM2cV9JSbg9UlSjZN4Uy5he4VDLMN/L1J2SN7XR4LKeFdr9PqFmI/HbCB6bBvgxpcBneTfpwrMGYo8y9HScLcVC1jmPWtVL1l3zQqp0cLGbqNraZ3oT9eEVM3xI/TDlOPEnGY+/Ed',s:[0xa87d26d135,0xc6d4f6bf8b],u:'/cdn-cgi/challenge-platform/h/g'}})();</script><script defer src="https://static.cloudflareinsights.com/beacon.min.js/v652eace1692a40cfa3763df669d7439c1639079717194" integrity="sha512-Gi7xpJR8tSkrpF7aordPZQlW2DLtzUlZcumS8dMQjwDHEnw9I7ZLyiOj/6tZStRBGtGgN6ceN6cMH8z7etPGlw==" data-cf-beacon='{"rayId":"71df4c682ebe9cec","token":"6e2c2575ac8f44ed824cef7899ba8463","version":"2022.6.0","si":100}' crossorigin="anonymous"></script>
@endsection