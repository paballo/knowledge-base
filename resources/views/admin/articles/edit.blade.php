@extends('/admin/layout/app')

@section('content')

<script src="https://cdn.tiny.cloud/1/ofq3esqpn0ltymb89297z0b11mfgzn10tolcmt6bqi8ohvff/tinymce/6/tinymce.min.js" referrerpolicy="origin"></script>
<main>
    <header class="page-header page-header-compact page-header-light border-bottom bg-white mb-4">
        <div class="container-xl px-4">
            <div class="page-header-content">
                <div class="row align-items-center justify-content-between pt-3">
                    <div class="col-auto mb-3">
                        <h1 class="page-header-title">
                            <div class="page-header-icon"><i class="fa-solid fa-pen-to-square"></i></div>
                            Edit Article
                        </h1>
                    </div>
                    <div class="col-12 col-xl-auto mb-3">
                        <a href="/admin/articles/show" class="btn btn-sm btn-light text-primary" >
                            <i class="fa fa-list"></i>
                            &nbsp; Manage Articles
                        </a>
    
                        <a href="/admin/articles/create" class="btn btn-sm btn-light text-primary" >
                            <i class="fa fa-plus"></i>
                            &nbsp; Create New Post
                        </a>
                    </div> 
                </div>
            </div>
        </div>
    </header>
    @if(Session::has('status'))
    <div class="alert alert-info" role="alert">
       <h1 style="color: green">{{Session::get('status')}}</h1>
    </div> 
   @endif 
    <!-- Main page content-->
    <div class="container-xl px-4 mt-4">
    
        <form action="/admin/articles/edit" method="post">
            @csrf
        <div class="row">
            <div class="col-xl-12">
                <!-- Profile picture card-->
                <input class="form-control" name="id" type="hidden" value="{{$article->id}}">

                <div class="card mb-4 mb-xl-0">
                    <div class="card-header">Aritcle Title</div>
                    <div class="card-body text-center">
                            <input class="form-control" name="title" type="text" value="{{$article->title}}">
                        </div>
                </div>
                 <br>
                <div class="card mb-4 mb-xl-0">
                    <div class="card-header">Article Content</div>
                    <div class="card-body text-center">
                       <textarea name="content" value="{!! $article->content !!}">{!! $article->content !!}</textarea>

                    </div>   
                </div>
                <br>
                <div class="col-xl-8">
                    <!-- Profile picture card-->
                    <div class="card mb-4 mb-xl-0 ">
                        <button class="btn btn-primary btn-lg" type="submit">Update</button>
                    </div>
                </div> 
            </form>
                <br>
                <br>  
            </div>
        </div>    
    </div>
    <script>
        tinymce.init({
          selector: 'textarea',
          plugins: 'preview importcss searchreplace autolink autosave save directionality code visualblocks visualchars fullscreen image link media template codesample table charmap pagebreak nonbreaking anchor insertdatetime advlist lists wordcount help charmap quickbars emoticons',
           toolbar: 'undo redo | bold italic underline strikethrough | fontfamily fontsize blocks | alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist | forecolor backcolor removeformat | pagebreak | charmap emoticons | fullscreen  preview save print | insertfile image media template link anchor codesample | ltr rtl',
          toolbar_mode: 'floating',
          tinycomments_mode: 'embedded',
          tinycomments_author: 'Author name',
        });
      </script>
</main>
@endsection