<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>knowledge-base</title>
    <link href="http://demo-knowledge-base.quickadminpanel.com/css/bootstrap.css" rel="stylesheet">
    <link href="http://demo-knowledge-base.quickadminpanel.com/css/font-awesome.min.css" rel="stylesheet">
    <link href="http://demo-knowledge-base.quickadminpanel.com/css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
    <div class="container featured-area-defualt padding-30">
        @yield('content')
    </div>  

        <script src="http://demo-knowledge-base.quickadminpanel.com/js/jquery-2.2.4.min.js"></script>
        <script src="http://demo-knowledge-base.quickadminpanel.com/js/app.js"></script>
        <script src="http://demo-knowledge-base.quickadminpanel.com/js/bootstrap.min.js"></script>
        <script src='https://cdn.rawgit.com/VPenkov/okayNav/master/app/js/jquery.okayNav.js'></script>
    </body>
    </html>