@extends('layouts.app')
@section('content')

<div class="row">
    <div class="col-md-8 padding-20">
        <div class="row">
            <ol class="breadcrumb">
                <li>
                    <a href="">
                        <i class="fa fa-home"></i>
                    </a>
                </li>
                <li>
                    <a href="/">knowledge-base</a>
                    <?php $cat = App\Models\Category::where('id', $article->category)->first(); ?>
                    <i class="active"> / {{$cat->slug}} </i>
                    <i class="active"> / {{$article->title}}</i>
                </li>
            </ol>
        </div>
        <hr class="stlye-three">
        <div class="panel panel-default">
            <div class="article-heading-abb">
                    <i class="fa fa-file-text-o"></i>{{$article->title}}
            </div>
            <div class="article-info">
                <div class="art-date">
                    <i class="fa fa-calendar-o"> {{ \Carbon\Carbon::parse($article->created_at)->diffForHumans()}}</i>
                </div>
                <div class="art-category">
                    <i class="fa fa-file-text-o"></i> {{$article->title}}
                </div>
            </div>
            <div class="article-content">
                <p class="block-with-text">{!! $article->content  !!}</p>
            </div>
        </div>
    </div>
</div>
@endsection