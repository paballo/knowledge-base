@extends('/layouts.index')

@section('content')

<main>
    <header class="page-header page-header-compact page-header-light border-bottom bg-white mb-4">
        <div class="container-fluid px-4">
            <div class="page-header-content">
                <div class="row align-items-center justify-content-between pt-3">
                    <div class="col-auto mb-3">
                        <h1 class="page-header-title">
                            <div class="page-header-icon"><i class="fa fa-list"></i></div>
                            ARTICLES LIST
                        </h1>
                    </div>
                    <div class="col-12 col-xl-auto mb-3">
                        
                        <a href="/mylearnings" class="btn btn-sm btn-light text-primary" >
                            <i class="fa fa-book"></i> &nbsp;
                            My Learning
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- Main page content-->
    <div class="container-fluid px-4">
        <div class="card">
            <div class="card-body">
                <table id="datatablesSimple">
                    <thead>
                        <tr>
                           <th>#</th>
                            <th>Articles</th>
                            <th>Number of Articles</th>
                            <th>Start Learning</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>#</th>
                            <th>Articles</th>
                            <th>Number of Articles</th>
                            <th>Start Learning</th>
                        </tr>
                    </tfoot>
                    <tbody>
                    <?php $catergories = App\Models\Category::select(['id','slug','created_at'])->get(); ?>
                    @if(count($catergories) > 0) {
                        @foreach ($catergories as $item)
                       <tr>
                         <td style="font-weight:bold;text-align:center;color:grey;font-size:25">0<?php 
                            static $num = 0;
                            echo (++$num)
                          ?>
                          </td>
                        <td> 
                            <i class="fa fa-folder" style="color: orange;font-size:15px">&nbsp; <span style="font-size:15px;text-transform: uppercase;color:black">{{$item->slug}}</span></i>
                        </td>
                        <td><?php
                            $count = DB::table('articles')
                            ->where('articles.category', '=', $item->id )
                            ->count();
                           ?>
                          <span class="cat-count">({{$count}})</span></td>

                          <td><a href="/categories/{{$item->id}}" class="btn bg-light">Start Learning</a></td>  
                    </tr>
                    @endforeach 
                }
                @else 
                @endif
                    </tbody>
                </table>
            </div>

        </div>
        
    </div>
    
</main>

@endsection