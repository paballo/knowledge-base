@extends('layouts.app')
@section('content')

<div class="row">
    <div class="col-md-8 padding-20">
        <div class="row">
            <ol class="breadcrumb">  
                <li>
                    <a href="/learner">
                        <i class="fa fa-home"></i>
                    </a>
                </li>
                <li>
                    <a href="/">knowledge-base</a>
                    <i class="active">/ {{$title->slug}}</i>
                </li>
            </ol>
        </div>
        @if(Session::has('status'))
     <div class="alert alert-info" role="alert">
        <h1 style="color: green">{{Session::get('status')}}</h1>
     </div> 
    @endif
 
        <div class="fb-heading">
            <i class="fa fa-folder-open"style="color: orange"> </i>cateragory : {{$title->slug}}
            <span class="cat-count"></span>
        </div>
        <hr class="stlye-three">
       @foreach ($article as $art)
        <div class="panel panel-default">
            <div class="article-heading-abb">
                <a href="/single_page/{{$art->id}}">
                    <i class="fa fa-file-text-o"></i>{{$art->title}}
                </a>
            </div>
            <div class="article-info">
                <div class="art-date">
                    <i class="fa fa-calendar-o"> {{ \Carbon\Carbon::parse($art->created_at)->diffForHumans()}}</i>
                </div>
                <div class="art-category">
                    <i class="fa fa-folder" style="color: orange"></i> <span style="text-transform:uppercase">{{$title->slug}}</span>
                </div>
                <div class="art-category">
                    <?php $badge = DB::table('users')
                     ->join('learns','users.id','learns.user_id')
                     ->get() ?>
                     @foreach($badge as $bad)
                     @if($bad->article_id ==  $art->id)
                    <span class="badge bg-info">Done</span>
                    @endif
                    @endforeach
                </div>
            </div>
            <div class="article-content">
                <p class="block-with-text">{!! substr($art->content, 0, 1000) !!}</p>
            </div>
            <div class="article-read-more">
                <a href="/single_page/{{$art->id}}" class="btn btn-default btn-wide">Read More ...</a>
            </div>
        </div>
       @endforeach
    </div>
</div>
@endsection