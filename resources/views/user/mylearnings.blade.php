@extends('/layouts.index')

@section('content')
<main>
    <header class="page-header page-header-compact page-header-light border-bottom bg-white mb-4">
        <div class="container-fluid px-4">
            <div class="page-header-content">
                <div class="row align-items-center justify-content-between pt-3">
                    <div class="col-auto mb-3">
                        <h1 class="page-header-title">
                            <div class="page-header-icon"><i class="fa fa-list"></i></div>
                            MY LEARNING
                        </h1>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- Main page content-->
    <div class="container-fluid px-4">
        <div class="card">
            <div class="card-body">
                <table id="datatablesSimple">
                    <thead>
                        <tr>
                           <th>#</th>
                            <th>Category</th>
                            <th>Number of Articles</th>
                            <th>Start-Date</th>
                            <th>End-Date</th>
                            <th>Total articles to read</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>#</th>
                            <th>Category</th>
                            <th>Number of Articles</th>
                            <th>Start-Date</th>
                            <th>End-Date</th>
                            <th>Total articles to read</th>
                        </tr>
                    </tfoot>
                    <tbody>
                       <tr>
                        <?php $mylearnings = DB::table('users')
                              ->join('records', 'users.id', '=', 'records.user_id')
                              ->join('categories', 'records.module_id', '=', 'categories.id')
                              ->where('records.user_id', '=', Auth::user()->id)
                              ->get(); 
                        ?>
                         @foreach ($mylearnings as $item)
                         <td style="font-weight:bold;text-align:center;color:grey;font-size:25">0<?php 
                            static $num = 0;
                            echo (++$num)
                          ?>
                          </td>
                        <td> <a href="/categories/{{$item->id}}">
                            <i class="fa fa-folder" style="color: orange;font-size:15px">&nbsp; <span style="font-size:15px;text-transform: uppercase;color:black">{{$item->slug}}</span></i>
                        </a></td>
                        <td><?php
                            $count = DB::table('articles')
                            ->where('articles.category', '=', $item->id )
                            ->count();
                           ?>
                          <span class="cat-count">({{$count}})</span></td>
                        <td>{{$item->start_date}}</td>
                        <td>{{$item->end_date}}</td>
                        @if($item->total_articles == 0) {
                            <td>You have complated the module</td>
                        }
                        @else {
                            <td>You have {{$item->total_articles}} articles to read</td>
                        }
                        @endif
                        @endforeach
                    </tr>
                    </tbody>
                </table>
            </div>

        </div>

    </div>
    
</main>

@endsection