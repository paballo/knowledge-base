import{_ as c}from"./AppLayout-8038efc1.js";import p from"./DeleteUserForm-8a9787cc.js";import l from"./LogoutOtherBrowserSessionsForm-51072104.js";import{S as s}from"./SectionBorder-66679e47.js";import f from"./TwoFactorAuthenticationForm-a0b83aaa.js";import u from"./UpdatePasswordForm-98649a10.js";import _ from"./UpdateProfileInformationForm-dd7b19b3.js";import{o,c as d,w as n,a as i,e as r,b as t,f as a,F as h}from"./app-9fc5d554.js";import"./_plugin-vue_export-helper-c27b6911.js";import"./DialogModal-aef9c39c.js";import"./SectionTitle-26b239aa.js";import"./DangerButton-af8aad7b.js";import"./TextInput-911cf44d.js";import"./SecondaryButton-f450439f.js";import"./ActionMessage-4ecdd814.js";import"./PrimaryButton-f9f5f7f6.js";import"./InputLabel-05b8b0d7.js";import"./FormSection-5a301e9f.js";const g=i("h2",{class:"font-semibold text-xl text-gray-800 leading-tight"}," Profile ",-1),$={class:"max-w-7xl mx-auto py-10 sm:px-6 lg:px-8"},w={key:0},k={key:1},y={key:2},z={__name:"Show",props:{confirmsTwoFactorAuthentication:Boolean,sessions:Array},setup(m){return(e,x)=>(o(),d(c,{title:"Profile"},{header:n(()=>[g]),default:n(()=>[i("div",null,[i("div",$,[e.$page.props.jetstream.canUpdateProfileInformation?(o(),r("div",w,[t(_,{user:e.$page.props.auth.user},null,8,["user"]),t(s)])):a("",!0),e.$page.props.jetstream.canUpdatePassword?(o(),r("div",k,[t(u,{class:"mt-10 sm:mt-0"}),t(s)])):a("",!0),e.$page.props.jetstream.canManageTwoFactorAuthentication?(o(),r("div",y,[t(f,{"requires-confirmation":m.confirmsTwoFactorAuthentication,class:"mt-10 sm:mt-0"},null,8,["requires-confirmation"]),t(s)])):a("",!0),t(l,{sessions:m.sessions,class:"mt-10 sm:mt-0"},null,8,["sessions"]),e.$page.props.jetstream.hasAccountDeletionFeatures?(o(),r(h,{key:3},[t(s),t(p,{class:"mt-10 sm:mt-0"})],64)):a("",!0)])])]),_:1}))}};export{z as default};
