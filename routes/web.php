<?php

use Inertia\Inertia;
use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Application;
use App\Http\Controllers\ArticlesController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\LearnController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified',
])->group(function () {
    
});

Route::get('/', [ArticlesController::class, 'index']);

Route::get('/admin', [ArticlesController::class, 'admin_index'])->middleware(['auth', 'verified', 'admin'])->name('admin');

Route::get('/admin/articles/show', [ArticlesController::class, 'articles_index'])->middleware(['auth', 'verified', 'admin']);
Route::get('/admin/articles/create', [ArticlesController::class, 'article'])->middleware(['auth', 'verified', 'admin']);
Route::post('/admin/articles/create', [ArticlesController::class, 'article_create'])->middleware(['auth', 'verified', 'admin']);
Route::get('/admin/articles/edit/{id}', [ArticlesController::class, 'article_edit'])->middleware(['auth', 'verified', 'admin']);
Route::post('/admin/articles/edit', [ArticlesController::class, 'update'])->middleware(['auth', 'verified', 'admin'])->name('update');
Route::get('/admin/articles/{id}', [ArticlesController::class, 'destroy'])->middleware(['auth', 'verified', 'admin']);

Route::get('/admin/categories/show', [CategoryController::class, 'category_index'])->middleware(['auth', 'verified', 'admin']);
Route::get('/admin/categories/create', [CategoryController::class, 'index'])->middleware(['auth', 'verified', 'admin']);
Route::post('/admin/categories/create', [CategoryController::class, 'category_create'])->middleware(['auth', 'verified', 'admin'])->name('create');
Route::get('/admin/categories/edit/{id}', [CategoryController::class, 'category_edit'])->middleware(['auth', 'verified', 'admin']);
Route::post('/admin/categorie/edit', [CategoryController::class, 'update'])->middleware(['auth', 'verified', 'admin'])->name('edit');
Route::get('/admin/categories/{id}', [CategoryController::class, 'destroy'])->middleware(['auth', 'verified', 'admin']);

Route::get('/learner', [LearnController::class, 'index'])->middleware(['auth'])->name('learner');
Route::get('/categories/{id}', [LearnController::class, 'module'])->middleware(['auth'])->name('categories');
Route::get('/single_page/{id}', [LearnController::class, 'singlepage'])->middleware(['auth'])->name('single_page');
Route::get('/mylearnings', [LearnController::class, 'mylearnings'])->middleware(['auth'])->name('mylearnings');

